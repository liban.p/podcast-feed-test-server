FROM http:2.4

COPY ./html/* /usr/local/apache2/htdocs/
COPY ./apache_conf/* /usr/local/apache2/conf/

RUN sed -i '/^DocumentRoot/d' /usr/local/apache2/conf/httpd.conf
RUN mv /usr/local/apache2/conf/httpd.conf /usr/local/apache2/conf/httpd_base.conf
RUN cat /usr/local/apache2/conf/httpd_base.conf /usr/local/apache2/conf/httpd_virtualhosts.conf \
       > /usr/local/apache2/conf/httpd.conf



